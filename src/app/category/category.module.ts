import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListCategoryComponent} from './containers/list-category/list-category.component';
import {CategoryItemComponent} from './components/category-item/category-item.component';


@NgModule({
  declarations: [ListCategoryComponent, CategoryItemComponent],
  imports: [
    CommonModule
  ]
})
export class CategoryModule { }
