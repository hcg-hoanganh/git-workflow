import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ProductListComponent} from './containers/product-list/product-list.component';
import {ProductItemComponent} from './components/product-item/product-item.component';


@NgModule({
  declarations: [ProductListComponent, ProductItemComponent],
  imports: [
    CommonModule
  ]
})
export class ProductModule {}
